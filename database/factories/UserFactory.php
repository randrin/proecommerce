<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    $name = $faker->name;
    return [
        'username' => $faker->unique()->username,
        'name' => $name,
        'slug' => str_slug($name),
        'dateOfBorn' => $faker->dateTimeBetween('-30 years', 'now'),
        'provider_id' => $faker->randomNumber,
        'provider' => $faker->realText(rand(10, 15)),
        'email' => $faker->unique()->safeEmail,
        'sex' => $faker->randomElement($array = array('male', 'female')),
        'status_user' => $faker->boolean,
        'avatar' => $faker->imageUrl($width = 400, $height = 400),
        'avatarcover' => $faker->imageUrl,
        'phone' => $faker->phoneNumber,
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
        'country' => $faker->country($faker->countryCode),
        'city' => $faker->city(),
        'state' => $faker->streetName(),
        'postcode' => $faker->postcode(),
        'email_verified_at' => now(),
        'remember_token' => Str::random(10),
    ];
});
