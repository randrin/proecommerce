<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Coupon;
use Faker\Generator as Faker;


$factory->define(Coupon::class, function (Faker $faker) {
    $name = str_random(8);
    return [
        'name' => $name,
        'slug' => str_slug($name),
        'discount' => $faker->numberBetween(0,100),
        'status' => $faker->boolean,
    ];
});
