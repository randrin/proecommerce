<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Admin;
use Faker\Generator as Faker;

$factory->define(Admin::class, function (Faker $faker) {
    $name = $faker->name;
    return [
        'username' => $faker->unique()->username,
        'name' => $name,
        'slug' => str_slug($name),
        'email' => $faker->unique()->safeEmail,
        'sex' => $faker->randomElement($array = array('male', 'female')),
        'status_admin' => $faker->boolean,
        'avatar' => $faker->imageUrl($width = 400, $height = 400),
        'avatarcover' => $faker->imageUrl,
        'phone' => $faker->phoneNumber,
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
    ];
});
