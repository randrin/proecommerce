<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->addDefaultUsers();

        if (config('app.env') !== 'production') {
            $this->addTestUsers();
        }

        // Fake Users Data
        factory(User::class, 10)->create();
    }

    private function addDefaultUsers()
    {
        // Truncate table
        Schema::disableForeignKeyConstraints();
        User::truncate();
        Schema::enableForeignKeyConstraints();

        User::create([
            'username' => 'bokino12',
            'name' => 'Boclair Temgoua',
            'slug' => 'boclair-temgoua',
            'dateOfBorn' => '1990-01-12',
            'provider_id' => 123456789,
            'provider' => 'ProEcommerce',
            'email' => "temgoua2012@gmail.com",
            'status_user' => '1',
            'phone' => '32698554789',
            'country' => 'Italy',
            'city' => 'Milano',
            'state' => 'Via Giambellino 42',
            'postcode' => '23147',
            "password" => bcrypt('0000000'),
            'created_at' => now(),
            'email_verified_at' => now(),
        ]);

        User::create([
            'username' => 'randrino17',
            'name' => 'Nzeukang Randrin',
            'slug' => 'nzeukang-randrin',
            'dateOfBorn' => '1986-12-17',
            'provider_id' => 123456789,
            'provider' => 'ProEcommerce',
            'email' => "nzeukangrandrin@gmail.com",
            'status_user' => '1',
            'phone' => '3296187465',
            'country' => 'Italy',
            'city' => 'Vigevano',
            'state' => 'Via Santa Maria 31/7',
            'postcode' => '27029',
            "password" => bcrypt('123456789'),
            'created_at' => now(),
            'email_verified_at' => now(),
        ]);
    }

    private function addTestUsers()
    {
        // Users Seeds
        User::create([
            'username' => 'patrick96',
            'name' => 'Darry Noubissi',
            'slug' => 'darry-noubissi',
            'dateOfBorn' => '1993-05-25',
            'provider_id' => 123456789,
            'provider' => 'ProEcommerce',
            'email' => "darrytafeng@gmail.com",
            'status_user' => '0',
            'phone' => '31569874568',
            'country' => 'Italy',
            'city' => 'Milano',
            'state' => 'Via Giambellino 42',
            'postcode' => '23147',
            "password" => bcrypt('0000000'),
            'created_at' => now(),
            'email_verified_at' => now(),
        ]);

        // Output
        $this->command->info('Users data added.');
    }
}
