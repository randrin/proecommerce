<?php

use App\Model\Coupon;
use Illuminate\Database\Seeder;

class CouponsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Fake Users Data
        factory(Coupon::class, 5)->create();

        // Output
        $this->command->info('Coupons data added.');
    }
}
