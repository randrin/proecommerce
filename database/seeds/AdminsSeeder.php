<?php

use App\Admin;
use Illuminate\Database\Seeder;

class AdminsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (config('app.env') !== 'production') {
            $this->addTestAdmins();
        }

        // Fake Users Data
        factory(Admin::class, 5)->create();
    }

    public function addTestAdmins() {

        // Admins Seeds
        Admin::create([
            'username' => 'bokino12',
            'name' => 'Boclair Temgoua',
            'slug' => 'boclair-temgoua',
            'email' => "temgoua2012@gmail.com",
            'status_admin' => '1',
            'phone' => '32698554789',
            "password" => bcrypt('0000000'),
            'created_at' => now(),
        ]);

        Admin::create([
            'username' => 'randrino17',
            'name' => 'Nzeukang Randrin',
            'slug' => 'nzeukang-randrin',
            'email' => "nzeukangrandrin@gmail.com",
            'status_admin' => '1',
            'phone' => '3296187465',
            "password" => bcrypt('123456789'),
            'created_at' => now(),
        ]);

        // Output
        $this->command->info('Admins data added.');
    }
}
