<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username')->unique()->nullable();
            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->date('dateOfBorn')->nullable();
            $table->integer('provider_id')->nullable();
            $table->string('provider')->nullable();
            $table->string('email')->unique();
            $table->string('sex')->default('Male');
            $table->string('status_user')->nullable()->default('0');
            $table->string('avatar')->nullable()->default('/assets/images/default-avatar.png');
            $table->string('avatarcover')->nullable()->default('/assets/images/image_placeholder.jpg');
            $table->string('phone')->nullable();
            $table->string('password');
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('postcode')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
