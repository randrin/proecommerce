<?php

Route::group(['namespace' => 'BackOffice'], function () {
    Route::get('/backoffice/categories', 'CategoryController@index')->name('backoffice.category.index');
    Route::post('/backoffice/category/save', 'CategoryController@store')->name('backoffice.category.store');
    Route::get('/backoffice/category/edit/{id}', 'CategoryController@edit')->name('backoffice.category.edit');
    Route::post('/backoffice/category/update/{id}', 'CategoryController@update')->name('backoffice.category.update');
    Route::get('/backoffice/category/delete/{id}', 'CategoryController@destroy')->name('backoffice.category.delete');
    Route::get('/backoffice/category/activate/{id}', 'CategoryController@activate')->name('backoffice.category.activate');
    Route::get('/backoffice/category/desactivate/{id}', 'CategoryController@desactivate')->name('backoffice.category.desactivate');
});