<?php

Route::group(['namespace' => 'BackOffice'], function () {
    Route::get('/backoffice/products', 'ProductController@index')->name('backoffice.product.index');
    Route::get('/backoffice/product/create', 'ProductController@create')->name('backoffice.product.create');
    Route::post('/backoffice/product/store', 'ProductController@store')->name('backoffice.product.store');
    Route::get('/backoffice/product/edit/{id}', 'ProductController@edit')->name('backoffice.product.edit');
    Route::post('/backoffice/product/update/{id}', 'ProductController@update')->name('backoffice.product.update');
    Route::get('/backoffice/product/show/{id}', 'ProductController@show')->name('backoffice.product.show');
    Route::get('/backoffice/product/delete/{id}', 'ProductController@destroy')->name('backoffice.product.delete');
    Route::get('/backoffice/product/activate/{id}', 'ProductController@activate')->name('backoffice.product.activate');
    Route::get('/backoffice/product/desactivate/{id}', 'ProductController@desactivate')->name('backoffice.product.desactivate');
});
