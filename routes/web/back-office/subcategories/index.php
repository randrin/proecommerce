<?php

Route::group(['namespace' => 'BackOffice'], function () {
    Route::get('/backoffice/subcategories', 'SubcategoryController@index')->name('backoffice.subcategory.index');
    Route::get('/backoffice/get/subcategories/{category_id}', 'SubcategoryController@getSubcategoriesByCategory')->name('backoffice.subcategory.getSubcategoriesByCategory');
    Route::post('/backoffice/subcategories/save', 'SubcategoryController@store')->name('backoffice.subcategory.store');
    Route::get('/backoffice/subcategories/edit/{id}', 'SubcategoryController@edit')->name('backoffice.subcategory.edit');
    Route::post('/backoffice/subcategories/update/{id}', 'SubcategoryController@update')->name('backoffice.subcategory.update');
    Route::get('/backoffice/subcategories/delete/{id}', 'SubcategoryController@destroy')->name('backoffice.subcategory.delete');
    Route::get('/backoffice/subcategories/activate/{id}', 'SubcategoryController@activate')->name('backoffice.subcategory.activate');
    Route::get('/backoffice/subcategories/desactivate/{id}', 'SubcategoryController@desactivate')->name('backoffice.subcategory.desactivate');
});
