<?php

/** Admin **/
require(__DIR__ . DIRECTORY_SEPARATOR . 'admin' . DIRECTORY_SEPARATOR . 'index.php');

/** Categories **/
require(__DIR__ . DIRECTORY_SEPARATOR . 'categories' . DIRECTORY_SEPARATOR . 'index.php');

/** Brands **/
require(__DIR__ . DIRECTORY_SEPARATOR . 'brands' . DIRECTORY_SEPARATOR . 'index.php');

/** Sub Categories **/
require(__DIR__ . DIRECTORY_SEPARATOR . 'subcategories' . DIRECTORY_SEPARATOR . 'index.php');

/** Coupons **/
require(__DIR__ . DIRECTORY_SEPARATOR . 'coupons' . DIRECTORY_SEPARATOR . 'index.php');

/** Newsleters **/
require(__DIR__ . DIRECTORY_SEPARATOR . 'newsletters' . DIRECTORY_SEPARATOR . 'index.php');

/** Products **/
require(__DIR__ . DIRECTORY_SEPARATOR . 'products' . DIRECTORY_SEPARATOR . 'index.php');