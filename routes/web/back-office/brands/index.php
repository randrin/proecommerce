<?php

Route::group(['namespace' => 'BackOffice'], function () {
    Route::get('/backoffice/brands', 'BrandController@index')->name('backoffice.brand.index');
    Route::post('/backoffice/brand/save', 'BrandController@store')->name('backoffice.brand.store');
    Route::get('/backoffice/brand/edit/{id}', 'BrandController@edit')->name('backoffice.brand.edit');
    Route::post('/backoffice/brand/update/{id}', 'BrandController@update')->name('backoffice.brand.update');
    Route::get('/backoffice/brand/delete/{id}', 'BrandController@destroy')->name('backoffice.brand.delete');
    Route::get('/backoffice/brand/activate/{id}', 'BrandController@activate')->name('backoffice.brand.activate');
    Route::get('/backoffice/brand/desactivate/{id}', 'BrandController@desactivate')->name('backoffice.brand.desactivate');
});
