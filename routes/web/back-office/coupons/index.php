<?php

Route::group(['namespace' => 'BackOffice'], function () {
    Route::get('/backoffice/coupons', 'CouponController@index')->name('backoffice.coupon.index');
    Route::post('/backoffice/coupon/save', 'CouponController@store')->name('backoffice.coupon.store');
    Route::get('/backoffice/coupon/edit/{id}', 'CouponController@edit')->name('backoffice.coupon.edit');
    Route::post('/backoffice/coupon/update/{id}', 'CouponController@update')->name('backoffice.coupon.update');
    Route::get('/backoffice/coupon/delete/{id}', 'CouponController@destroy')->name('backoffice.coupon.delete');
    Route::get('/backoffice/coupon/activate/{id}', 'CouponController@activate')->name('backoffice.coupon.activate');
    Route::get('/backoffice/coupon/desactivate/{id}', 'CouponController@desactivate')->name('backoffice.coupon.desactivate');
});
