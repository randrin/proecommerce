<?php

Route::group(['namespace' => 'BackOffice'], function () {
    Route::group(['namespace' => 'Partials'], function () {
        Route::get('/backoffice/newsletters', 'NewsletterController@index')->name('backoffice.newsletter.index');
        Route::post('/backoffice/newsletter/save', 'NewsletterController@store')->name('backoffice.newsletter.store');
        Route::get('/backoffice/newsletter/delete/{id}', 'NewsletterController@destroy')->name('backoffice.newsletter.delete');
    });
});
