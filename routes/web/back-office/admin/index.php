
<?php

Route::group(['namespace' => 'Admin'], function () {
        Route::get('/admin/home', 'AdminController@index')->name('admin.index');
        Route::get('/admin', 'LoginController@showLoginForm')->name('admin.login.form');
        Route::post('/admin', 'LoginController@login')->name('admin.login');
        Route::get('/admin/change/password', 'ChangePasswordController@changePassword')->name('admin.change.password');
        Route::post('/admin/update/password', 'ChangePasswordController@updatePassword')->name('admin.update.password');
        Route::get('/admin/logout', 'AdminController@logout')->name('admin.logout');
});