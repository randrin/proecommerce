@extends('inc.back-office.admin.main')
<?php $item = htmlspecialchars(config('app.name'));?>
@section('title', 'Edit Coupon | '.$coupon->name)

@section('style')
@endsection

@section('init')
    <!-- Site wrapper -->
@endsection

@section('content')
<div class="sl-mainpanel">
      <nav class="breadcrumb sl-breadcrumb">
        <a class="breadcrumb-item" href="{{ route('admin.index') }}">ProEcommerce</a>
        <span class="breadcrumb-item active">Edit Coupon</span>
      </nav>

      <div class="sl-pagebody">
        <div class="card pd-20 pd-sm-40">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
          <h6 class="card-body-title mb-5">
              <span>Update Coupon Ecommerce</span>
            </h6>
            <form method="POST" action="{{ route('backoffice.coupon.update', $coupon->id) }}">
            @csrf
              <div class="form-group">
                <label for="name" class="col-form-label">Coupon Name</label>
                <input type="text" style="text-transform: uppercase;" value="{{ strtoupper($coupon->name) }}" placeholder="Enter the coupon name" name="name" class="form-control" id="name">
              </div>
              <div class="form-group">
                <label for="discount" class="col-form-label">Category Discount (%)</label>
                <input type="number" value="{{ $coupon->discount }}" placeholder="Enter the coupon icon" name="discount" class="form-control" id="discount">
              </div>
              <div class="mt-5 pull-right">
                  <button type="submit" class="btn btn-info pd-x-20">Update</button>
                  <a href="{{ route('backoffice.coupon.index') }}" class="btn btn-secondary pd-x-20">Back</a>
              </div>
            </form>
        </div>
      </div>
    </div>
    @endsection

@section('script')

@endsection