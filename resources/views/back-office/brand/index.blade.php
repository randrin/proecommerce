@extends('inc.back-office.admin.main')
<?php $item = htmlspecialchars(config('app.name'));?>
@section('title', 'Dashboard Brands | '.$item)

@section('style')
@endsection

@section('init')
    <!-- Site wrapper -->
@endsection

@section('content')
    <div class="sl-mainpanel">
      <nav class="breadcrumb sl-breadcrumb">
        <a class="breadcrumb-item" href="{{ route('admin.index') }}">ProEcommerce</a>
        <span class="breadcrumb-item active">All Brands</span>
      </nav>

      <div class="sl-pagebody">
        <div class="card pd-20 pd-sm-40">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
          <h6 class="card-body-title mb-5">
              <span>Brands Ecommerce</span>
                <a class="btn btn-sm btn-success pull-right" data-toggle="modal" data-target="#new-brand">
                  <i class="icon ion-ios-pie-outline tx-15"></i>
                  <span>New Brand</span>
                </a>
            </h6>

          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">Brand Logo</th>
                  <th class="wd-15p">Brand Name</th>
                  <th class="wd-15p">Brand Status</th>
                  <th class="wd-15p">Created Date</th>
                  <th class="wd-10p">Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($brands as $brand)
                <tr>
                  <td><img src="{{$brand->logo}}" height="50" width="80" style="border-radius: 5px;" /></td>
                  <td>{{$brand->name}}</td>
                  @if($brand->status == '0')
                    <td><span class="badge badge-danger">Desactive</span></td>
                  @else 
                    <td><span class="badge badge-success">Active</span></td>
                  @endif
                  <td>{{$brand->created_at->format('d/m/Y')}}</td>
                  <td class="d-flex">
                    @if($brand->status == '0')
                      <a href="{{ route('backoffice.brand.activate', $brand->id) }}" class="btn btn-sm btn-success mr-3">
                        <i class="icon ion-power tx-15"></i>
                        <span>Activate</span>
                      </a>
                    @else 
                      <a href="{{ route('backoffice.brand.desactivate', $brand->id) }}" class="btn btn-sm btn-danger mr-3">
                        <i class="icon ion-power tx-15"></i>
                        <span>Desactivate</span>
                      </a>
                    @endif
                    <a href="{{ route('backoffice.brand.edit', $brand->id) }}" class="btn btn-sm btn-primary mr-3">
                      <i class="icon ion-edit tx-15"></i>
                      <span>Edit</span>
                    </a>
                    <a href="{{ route('backoffice.brand.delete', $brand->id) }}" class="btn btn-sm btn-warning" id="delete">
                      <i class="ion-ios-trash tx-15"></i>
                      <span>Delete</span>
                    </a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
    </div>

    
    <!-- CREATE NEW  BRAND -->
    <div id="new-brand" class="modal fade">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content tx-size-sm">
          <div class="modal-header pd-x-20">
            <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">New Brand</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body pd-20">
            <form method="POST" action="{{ route('backoffice.brand.store') }}" enctype="multipart/form-data">
            @csrf
              <div class="form-group">
                <label for="name" class="col-form-label">Brand Name</label>
                <input type="text" placeholder="Enter the brand name" name="name" class="form-control" id="name">
              </div>
              <div class="form-group">
                <label for="logo" class="col-form-label">Brand Logo</label>
                <input type="file" placeholder="Enter the brand logo" name="logo" class="form-control" id="logo">
              </div>
              <div class="mt-5 pull-right">
                  <button type="submit" class="btn btn-info pd-x-20">Save</button>
                  <button type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">Close</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    </div>

@endsection

@section('script')

@endsection