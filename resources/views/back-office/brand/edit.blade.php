@extends('inc.back-office.admin.main')
<?php $item = htmlspecialchars(config('app.name'));?>
@section('title', 'Edit Brand | '.$brand->name)

@section('style')
@endsection

@section('init')
    <!-- Site wrapper -->
@endsection

@section('content')
<div class="sl-mainpanel">
      <nav class="breadcrumb sl-breadcrumb">
        <a class="breadcrumb-item" href="{{ route('admin.index') }}">ProEcommerce</a>
        <span class="breadcrumb-item active">All Brands</span>
      </nav>

      <div class="sl-pagebody">
        <div class="card pd-20 pd-sm-40">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
          <h6 class="card-body-title mb-5">
              <span>Update Brand Ecommerce</span>
            </h6>
            <form method="POST" action="{{ route('backoffice.brand.update', $brand->id) }}" enctype="multipart/form-data">
            @csrf
              <div class="form-group">
                <label for="name" class="col-form-label">Brand Name</label>
                <input type="text" value="{{ $brand->name }}" placeholder="Enter the brand name" name="name" class="form-control" id="name">
              </div>
              <div class="form-group text-center m-5">
                <img src="{{ $brand->logo }}" width="250" height="250" style="border-radius: 5px;" />
                <input type="file" placeholder="Enter the brand logo" name="logo" class="form-control mt-4" id="logo">
              </div>
              <div class="mt-5 pull-right">
                  <button type="submit" class="btn btn-info pd-x-20">Update</button>
                  <a href="{{ route('backoffice.brand.index') }}" class="btn btn-secondary pd-x-20">Back</a>
              </div>
            </form>
        </div>
      </div>
    </div>
    @endsection

@section('script')

@endsection