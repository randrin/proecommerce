@extends('inc.back-office.admin.main')
<?php $item = htmlspecialchars(config('app.name'));?>
@section('title', 'Dashboard Sub Categories | '.$item)

@section('style')
@endsection

@section('init')
    <!-- Site wrapper -->
@endsection

@section('content')
    <div class="sl-mainpanel">
      <nav class="breadcrumb sl-breadcrumb">
        <a class="breadcrumb-item" href="{{ route('admin.index') }}">ProEcommerce</a>
        <span class="breadcrumb-item active">All Sub Categories</span>
      </nav>

      <div class="sl-pagebody">
        <div class="card pd-20 pd-sm-40">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
          <h6 class="card-body-title mb-5">
              <span>Sub Categories Ecommerce</span>
                <a class="btn btn-sm btn-success pull-right" data-toggle="modal" data-target="#new-subcategory">
                  <i class="icon ion-ios-pie-outline tx-15"></i>
                  <span>New Sub-Category</span>
                </a>
            </h6>

          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">SubCategory Name</th>
                  <th class="wd-15p">Category</th>
                  <th class="wd-15p">SubCategory Status</th>
                  <th class="wd-15p">Created Date</th>
                  <th class="wd-10p">Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($subcategories as $subcategory)
                <tr>
                  <td>{{$subcategory->name}}</td>
                  <td>{{$subcategory->category_name}}</td>
                  @if($subcategory->status == '0')
                    <td><span class="badge badge-danger">Desactive</span></td>
                  @else 
                    <td><span class="badge badge-success">Active</span></td>
                  @endif
                  <td>{{$subcategory->created_at->format('d/m/Y')}}</td>
                  <td class="d-flex">
                    @if($subcategory->status == '0')
                      <a href="{{ route('backoffice.subcategory.activate', $subcategory->id) }}" class="btn btn-sm btn-success mr-3">
                        <i class="icon ion-power tx-15"></i>
                        <span>Activate</span>
                      </a>
                    @else 
                      <a href="{{ route('backoffice.subcategory.desactivate', $subcategory->id) }}" class="btn btn-sm btn-danger mr-3">
                        <i class="icon ion-power tx-15"></i>
                        <span>Desactivate</span>
                      </a>
                    @endif
                    <a href="{{ route('backoffice.subcategory.edit', $subcategory->id) }}" class="btn btn-sm btn-primary mr-3">
                      <i class="icon ion-edit tx-15"></i>
                      <span>Edit</span>
                    </a>
                    <a href="{{ route('backoffice.subcategory.delete', $subcategory->id) }}" class="btn btn-sm btn-warning" id="delete">
                      <i class="ion-ios-trash tx-15"></i>
                      <span>Delete</span>
                    </a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
    </div>

    
    <!-- CREATE NEW  SUB CATEGORY -->
    <div id="new-subcategory" class="modal fade">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content tx-size-sm">
          <div class="modal-header pd-x-20">
            <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">New Sub Category</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body pd-20">
            <form method="POST" action="{{ route('backoffice.subcategory.store') }}">
            @csrf
              <div class="form-group">
                <label for="name" class="col-form-label">Sub Category Name</label>
                <input type="text" placeholder="Enter the sub category name" name="name" class="form-control" id="name">
              </div>
              <div class="form-group">
                <label for="category_id" class="col-form-label">Category Name</label>
                <select class="form-control select2" name="category_id" data-placeholder="Choose Browser">
                    @foreach ($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>
              </div>
              <div class="mt-5 pull-right">
                  <button type="submit" class="btn btn-info pd-x-20">Save</button>
                  <button type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">Close</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    </div>

@endsection

@section('script')

@endsection