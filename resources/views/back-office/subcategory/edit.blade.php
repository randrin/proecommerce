@extends('inc.back-office.admin.main')
<?php $item = htmlspecialchars(config('app.name'));?>
@section('title', 'Edit Sub Category | '.$subcategory->name)

@section('style')
@endsection

@section('init')
    <!-- Site wrapper -->
@endsection

@section('content')
<div class="sl-mainpanel">
      <nav class="breadcrumb sl-breadcrumb">
        <a class="breadcrumb-item" href="{{ route('admin.index') }}">ProEcommerce</a>
        <span class="breadcrumb-item active">Edit Sub Category</span>
      </nav>

      <div class="sl-pagebody">
        <div class="card pd-20 pd-sm-40">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
          <h6 class="card-body-title mb-5">
              <span>Update Category Ecommerce</span>
            </h6>
            <form method="POST" action="{{ route('backoffice.subcategory.update', $subcategory->id) }}">
            @csrf
              <div class="form-group">
                <label for="name" class="col-form-label">Sub Category Name</label>
                <input type="text" value="{{ $subcategory->name }}" placeholder="Enter the category name" name="name" class="form-control" id="name">
              </div>
              <div class="form-group">
                <label for="category_id" class="col-form-label">Category Name</label>
                <select class="form-control select2" name="category_id" data-placeholder="Choose Browser">
                    @foreach ($categories as $category)
                        <option value="{{$category->id}}" <?php if($subcategory->category_id == $category->id) { echo "selected";} ?>>{{$category->name}}</option>
                    @endforeach
                </select>
              </div>
              <div class="mt-5 pull-right">
                  <button type="submit" class="btn btn-info pd-x-20">Update</button>
                  <a href="{{ route('backoffice.subcategory.index') }}" class="btn btn-secondary pd-x-20">Back</a>
              </div>
            </form>
        </div>
      </div>
    </div>
    @endsection

@section('script')

@endsection