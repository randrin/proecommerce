@extends('inc.back-office.admin.main')
<?php $item = htmlspecialchars(config('app.name'));?>
@section('title', 'Dashboard Newsletters | '.$item)

@section('style')
@endsection

@section('init')
    <!-- Site wrapper -->
@endsection

@section('content')
    <div class="sl-mainpanel">
      <nav class="breadcrumb sl-breadcrumb">
        <a class="breadcrumb-item" href="{{ route('admin.index') }}">ProEcommerce</a>
        <span class="breadcrumb-item active">All Newsletters</span>
      </nav>

      <div class="card pd-20 pd-sm-40">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
          <h6 class="card-body-title mb-5">
              <span>Newsletters Ecommerce</span>
            </h6>

          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">Email</th>
                  <th class="wd-15p">IP</th>
                  <th class="wd-15p">Email Submitted Date</th>
                  <th class="wd-10p">Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($newsletters as $newsletter)
                <tr>
                  <td>{{$newsletter->email}}</td>
                  <td>{{$newsletter->ip}}</td>
                  <td>{{$newsletter->created_at->format('d/m/Y')}}</td>
                  <td class="d-flex">
                    <a href="{{ route('backoffice.newsletter.delete', $newsletter->id) }}" class="btn btn-sm btn-warning" id="delete">
                      <i class="ion-ios-trash tx-15"></i>
                      <span>Delete</span>
                    </a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
    </div>

@endsection

@section('script')

@endsection