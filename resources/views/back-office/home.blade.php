@extends('inc.back-office.admin.main')
<?php $item = htmlspecialchars(config('app.name'));?>
@section('title', 'Admin | '.$item)

@section('style')
@endsection

@section('init')
    <!-- Site wrapper -->
@endsection

@section('content')
@guest
    @include('admin.auth.login')
@else
  @include('back-office.index')
@endguest

@endsection

@section('script')

@endsection