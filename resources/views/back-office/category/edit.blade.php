@extends('inc.back-office.admin.main')
<?php $item = htmlspecialchars(config('app.name'));?>
@section('title', 'Edit Category | '.$category->name)

@section('style')
@endsection

@section('init')
    <!-- Site wrapper -->
@endsection

@section('content')
<div class="sl-mainpanel">
      <nav class="breadcrumb sl-breadcrumb">
        <a class="breadcrumb-item" href="{{ route('admin.index') }}">ProEcommerce</a>
        <span class="breadcrumb-item active">Edit Category</span>
      </nav>

      <div class="sl-pagebody">
        <div class="card pd-20 pd-sm-40">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
          <h6 class="card-body-title mb-5">
              <span>Update Category Ecommerce</span>
            </h6>
            <form method="POST" action="{{ route('backoffice.category.update', $category->id) }}">
            @csrf
              <div class="form-group">
                <label for="name" class="col-form-label">Category Name</label>
                <input type="text" value="{{ $category->name }}" placeholder="Enter the category name" name="name" class="form-control" id="name">
              </div>
              <div class="form-group">
                <label for="icon" class="col-form-label">Category Icon</label>
                <input type="text" value="{{ $category->icon }}" placeholder="Enter the category icon" name="icon" class="form-control" id="icon">
              </div>
              <a href="https://ionicons.com/v2/cheatsheet.html" target="_blank">See the icons list there</a>
              <div class="mt-5 pull-right">
                  <button type="submit" class="btn btn-info pd-x-20">Update</button>
                  <a href="{{ route('backoffice.category.index') }}" class="btn btn-secondary pd-x-20">Back</a>
              </div>
            </form>
        </div>
        </div>
    </div>
    @endsection

@section('script')

@endsection