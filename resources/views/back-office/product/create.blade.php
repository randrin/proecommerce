@extends('inc.back-office.admin.main')
<?php $item = htmlspecialchars(config('app.name'));?>
@section('title', 'Create New Product | '.$item)

@section('style')
@endsection

@section('init')
    <!-- Site wrapper -->
@endsection

@section('content')
    <div class="sl-mainpanel">
      <nav class="breadcrumb sl-breadcrumb">
        <a class="breadcrumb-item" href="{{ route('admin.index') }}">ProEcommerce</a>
        <span class="breadcrumb-item active">New Product</span>
      </nav>

      <div class="sl-pagebody">
        <div class="card pd-20 pd-sm-40">
            <h6 class="card-body-title mb-5">
              <span>Add Product Ecommerce</span>
                <a class="btn btn-sm btn-success pull-right" href="{{ route('backoffice.product.index') }}">
                  <i class="icon ion-ios-pie-outline tx-15"></i>
                  <span>All Products</span>
                </a>
            </h6>
          <form method="POST" action="{{ route('backoffice.product.store') }}" enctype="multipart/form-data">
            @csrf
            <div class="form-layout">
            <div class="row mg-b-25">
              <div class="col-lg-4">
                <div class="form-group">
                  <label for="product_name" class="form-control-label">Product Name <span class="tx-danger">*</span></label>
                  <input class="form-control" id="product_name" type="text" name="product_name" placeholder="Enter the product name">
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label for="product_code" class="form-control-label">Product Code <span class="tx-danger">*</span></label>
                  <input class="form-control" id="product_code" type="text" readonly value="{{$product_code}}" name="product_code" placeholder="Enter the product code">
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label for="product_quantity" class="form-control-label">Product Quantity <span class="tx-danger">*</span></label>
                  <input class="form-control" id="product_quantity" type="number" name="product_quantity" placeholder="Enter product quantity">
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group mg-b-10-force">
                  <label for="category_id" class="form-control-label">Product Category <span class="tx-danger">*</span></label>
                  <select class="form-control select2" id="category_id" name="category_id" data-placeholder="Choose a category">
                    <option label="Choose a category"></option>
                    @foreach ($categories as $category)
                       <option value="{{$category->id}}">{{$category->name}}</option>
                   @endforeach
                  </select>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group mg-b-10-force">
                  <label for="subcategory_id" class="form-control-label">Product SubCategory <span class="tx-danger">*</span></label>
                  <select class="form-control select2" id="subcategory_id" name="subcategory_id" data-placeholder="Choose a subcategory">
                  </select>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group mg-b-10-force">
                  <label for="brand_id" class="form-control-label">Product Brand <span class="tx-danger">*</span></label>
                  <select class="form-control select2" id="brand_id" name="brand_id" data-placeholder="Choose a brand">
                   <option label="Choose a brand"></option>
                   @foreach ($brands as $brand)
                       <option value="{{$brand->id}}">{{$brand->name}}</option>
                   @endforeach
                  </select>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label for="product_size" class="form-control-label">Product Size <span class="tx-danger">*</span></label>
                  <input class="form-control" id="product_size" data-role="tagsinput" name="product_size" placeholder="Enter product size">
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label for="product_color" class="form-control-label">Product Color <span class="tx-danger">*</span></label>
                  <input class="form-control" id="product_color" data-role="tagsinput" name="product_color" placeholder="Enter product color">
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-2">
                <div class="form-group">
                  <label for="selling_price" class="form-control-label">Selling Price <span class="tx-danger">*</span></label>
                  <input class="form-control" id="selling_price" type="number" name="selling_price" placeholder="Enter the selling price">
                </div>
              </div><!-- col-2 -->
              <div class="col-lg-2">
                <div class="form-group">
                  <label for="discount_price" class="form-control-label">Discount Price </label>
                  <input class="form-control" id="discount_price" type="number" name="discount_price" placeholder="Enter the discount price">
                </div>
              </div><!-- col-2 -->
              <div class="col-lg-12">
                <div class="form-group">
                  <label for="product_details" class="form-control-label">Product Details <span class="tx-danger">*</span></label>
                  <textarea class="form-control" id="summernote" name="product_details" placeholder="Enter the selling price"></textarea>
                </div>
              </div><!-- col-12 -->
              <div class="col-lg-12">
                <div class="form-group">
                  <label for="video_link" class="form-control-label">Video Link</label>
                  <input class="form-control" type="text" id="video_link" name="video_link" placeholder="Enter Video Link">
                </div>
              </div><!-- col-12 -->
              <div class="col-lg-4">
                <div class="form-group" style="display: inline-grid">
                  <label for="image_one" class="form-control-label">Image One (Main of Product) <span class="tx-danger">*</span></label>
                    <label class="custom-file">
                        <input type="file" id="file" name="image_one" class="custom-file-input" onchange="readURL(this, 'image_one');" required="">
                        <span class="custom-file-control"></span>
                    </label>
                    <img src="#" id="image_one" style="display: none;" />
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group" style="display: inline-grid;">
                  <label for="image_two" class="form-control-label">Image Two <span class="tx-danger">*</span></label>
                    <label class="custom-file">
                        <input type="file" id="file" name="image_two" class="custom-file-input" onchange="readURL(this, 'image_two');" required="">
                        <span class="custom-file-control"></span>
                    </label>
                    <img src="#" id="image_two" style="display: none;" />
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group" style="display: inline-grid;">
                  <label for="image_three" class="form-control-label">Image Three <span class="tx-danger">*</span></label>
                    <label class="custom-file">
                        <input type="file" id="file" name="image_three" class="custom-file-input" onchange="readURL(this, 'image_three');" required="">
                        <span class="custom-file-control"></span>
                    </label>
                    <img src="#" id="image_three" style="display: none;" />
                </div>
              </div>
            </div><!-- row -->
            <hr />
            <div class="row my-5">
                <div class="col-lg-2">
                    <label class="ckbox">
                      <input type="checkbox" name="main_slider" value="1">
                        <span>Main Slider</span>
                    </label>
                </div>
                <div class="col-lg-2">
                    <label class="ckbox">
                      <input type="checkbox" name="hot_deal" value="1">
                        <span>Hot Deal</span>
                    </label>
                </div>
                <div class="col-lg-2">
                    <label class="ckbox">
                      <input type="checkbox" name="best_rated" value="1">
                        <span>Best Rated</span>
                    </label>
                </div>
                <div class="col-lg-2">
                    <label class="ckbox">
                      <input type="checkbox" name="mid_slider" value="1">
                        <span>Mid Slider</span>
                    </label>
                </div>
                <div class="col-lg-2">
                    <label class="ckbox">
                      <input type="checkbox" name="host_new" value="1">
                        <span>Host New</span>
                    </label>
                </div>
                <div class="col-lg-2">
                    <label class="ckbox">
                      <input type="checkbox" name="trend" value="1">
                        <span>Trend Product</span>
                    </label>
                </div>
            </div>
            <div class="form-layout-footer">
              <button type="submit" class="btn btn-info mg-r-5">Create Product</button>
              <a href="{{ route('backoffice.product.index') }}" class="btn btn-secondary">Cancel</a>
            </div><!-- form-layout-footer -->
          </div><!-- form-layout -->
          </form>
        </div><!-- card -->
      </div><!-- sl-pagebody -->
    </div>

@endsection

@section('script')
<script type="text/javascript">
  function readURL(input, image) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $(`#${image}`)
        .attr('src', e.target.result)
        .width(100)
        .height(100)
        .css({'top': '10px', 'position': 'relative', 'display': 'block', 'border-radius': '5px'})
      };
      reader.readAsDataURL(input.files[0]);
    }
  }
</script>

@endsection