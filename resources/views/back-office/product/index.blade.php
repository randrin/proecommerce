@extends('inc.back-office.admin.main')
<?php $item = htmlspecialchars(config('app.name'));?>
@section('title', 'Dashboard Products | '.$item)

@section('style')
@endsection

@section('init')
    <!-- Site wrapper -->
@endsection

@section('content')
    <div class="sl-mainpanel">
      <nav class="breadcrumb sl-breadcrumb">
        <a class="breadcrumb-item" href="{{ route('admin.index') }}">ProEcommerce</a>
        <span class="breadcrumb-item active">All Products</span>
      </nav>

      <div class="sl-pagebody">
        <div class="card pd-20 pd-sm-40">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
          <h6 class="card-body-title mb-5">
              <span>Products Ecommerce</span>
                <a class="btn btn-sm btn-success pull-right" href={{ route('backoffice.product.create')}}>
                  <i class="icon ion-ios-pie-outline tx-15"></i>
                  <span>New Product</span>
                </a>
            </h6>

          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">Product Code</th>
                  <th class="wd-15p">Product Name</th>
                  <th class="wd-15p">Image</th>
                  <th class="wd-15p">Category</th>
                  <th class="wd-15p">Brand</th>
                  <th class="wd-15p">Quantity</th>
                  <th class="wd-15p">Status</th>
                  <th class="wd-15p">Created Date</th>
                  <th class="wd-10p">Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($products as $product)
                <tr>
                  <td>{{$product->product_code}}</td>
                  <td>{{$product->product_name}}</td>
                  <td><img src="{{$product->image_one}}" height="50" width="80" style="border-radius: 5px;" /></td>
                  <td>{{$product->category_name}}</td>
                  <td>{{$product->brand_name}}</td>
                  <td>{{$product->product_quantity}}</td>
                  @if($product->status == '0')
                    <td><span class="badge badge-danger">Desactive</span></td>
                  @else 
                    <td><span class="badge badge-success">Active</span></td>
                  @endif
                  <td>{{$product->created_at->format('d/m/Y')}}</td>
                  <td class="d-flex">
                    @if($product->status == '0')
                      <a href="{{ route('backoffice.product.activate', $product->id) }}" class="btn btn-sm btn-success mr-3">
                        <i class="icon ion-power tx-15"></i>
                        <span>Activate</span>
                      </a>
                    @else 
                      <a href="{{ route('backoffice.product.desactivate', $product->id) }}" class="btn btn-sm btn-danger mr-3">
                        <i class="icon ion-power tx-15"></i>
                        <span>Desactivate</span>
                      </a>
                    @endif
                    <a href="{{ route('backoffice.product.show', $product->id) }}" class="btn btn-sm btn-secondary mr-3">
                      <i class="icon ion-eye tx-15"></i>
                      <span>View</span>
                    </a>
                    <a href="{{ route('backoffice.product.edit', $product->id) }}" class="btn btn-sm btn-primary mr-3">
                      <i class="icon ion-edit tx-15"></i>
                      <span>Edit</span>
                    </a>
                    <a href="{{ route('backoffice.product.delete', $product->id) }}" class="btn btn-sm btn-warning" id="delete">
                      <i class="ion-ios-trash tx-15"></i>
                      <span>Delete</span>
                    </a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

@endsection

@section('script')

@endsection