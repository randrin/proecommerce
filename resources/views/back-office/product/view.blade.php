@extends('inc.back-office.admin.main')
<?php $item = htmlspecialchars(config('app.name'));?>
@section('title', 'View Product | '.$item)

@section('style')
@endsection

@section('init')
    <!-- Site wrapper -->
@endsection

@section('content')
    <div class="sl-mainpanel">
      <nav class="breadcrumb sl-breadcrumb">
        <a class="breadcrumb-item" href="{{ route('admin.index') }}">ProEcommerce</a>
        <span class="breadcrumb-item active">View Product</span>
      </nav>

      <div class="sl-pagebody">
        <div class="card pd-20 pd-sm-40">
            <h6 class="card-body-title mb-5">
              <span>Add Product Ecommerce</span>
                <a class="btn btn-sm btn-success pull-right" href="{{ route('backoffice.product.index') }}">
                  <i class="icon ion-ios-pie-outline tx-15"></i>
                  <span>All Products</span>
                </a>
            </h6>
         
            <div class="form-layout">
            <div class="row">
              <div class="col-lg-4">
                <div class="form-group">
                  <label for="product_name" class="form-control-label"><b>Product Name</b></label><br/>
                  <span> {{$product->product_name}}</span>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label for="product_code" class="form-control-label"><b>Product Code</b></label><br/>
                  <span> {{$product->product_code}}</span>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label for="product_quantity" class="form-control-label"><b>Product Quantity </b></label><br/>
                  <span> {{$product->product_quantity}}</span>
                </div>
              </div><!-- col-4 -->
            </div>
            <hr/>
            <div class="row">
              <div class="col-lg-4">
                <div class="form-group mg-b-10-force">
                  <label for="category_id" class="form-control-label"><b>Product Category</b> </label><br/>
                  <span> {{$product->category_name}}</span>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group mg-b-10-force">
                  <label for="subcategory_id" class="form-control-label"><b>Product SubCategory </b></label><br/>
                   <span> {{$product->subcategory_name}}</span>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group mg-b-10-force">
                  <label for="brand_id" class="form-control-label"><b>Product Brand</b> </label><br/>
                  <span> {{$product->brand_name}}</span>
                </div>
              </div><!-- col-4 -->
            </div>
            <hr/>
            <div class="row">
              <div class="col-lg-4">
                <div class="form-group">
                  <label for="product_size" class="form-control-label"><b>Product Size</b> </label><br/>
                  <span> {{$product->product_size}}</span>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label for="product_color" class="form-control-label"><b>Product Color</b> </label><br/>
                    <span> {{$product->product_color}}</span>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-2">
                <div class="form-group">
                  <label for="selling_price" class="form-control-label"><b>Selling Price</b> </label><br/>
                  <span> {{$product->selling_price}}</span>
                </div>
              </div><!-- col-2 -->
              <div class="col-lg-2">
                <div class="form-group">
                  <label for="discount_price" class="form-control-label"><b>Discount Price</b> </label><br/>
                  @if($product->discount_price)
                  <span> {{$product->discount_price}}</span>
                  @else 
                  <span class="badge badge-info"> No Discount Price</span>
                  @endif
                </div>
              </div><!-- col-2 -->
            </div>
            <div>
               <hr/>
            <div class="row">
              <div class="col-lg-12">
                <div class="form-group">
                  <label for="product_details" class="form-control-label"><b>Product Details </b></label><br/>
                  <span> {{$product->product_details}}</span>
                </div>
              </div><!-- col-12 -->
            </div>
               <hr/>
            <div class="row">
              <div class="col-lg-12">
                <div class="form-group">
                  <label for="video_link" class="form-control-label"><b>Video Link</b></label><br/>
                  <span> {{$product->video_link}}</span>
                </div>
              </div><!-- col-12 -->
            </div>
               <hr/>
            <div class="row">
              <div class="col-lg-4">
                <div class="form-group" style="display: inline-grid">
                  <label for="image_one" class="form-control-label"><b>Image One (Main of Product)</b> </label><br/>
                    <img src="{{$product->image_one}}" height="100" width="100" style="border-radius: 5px;" />
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group" style="display: inline-grid;">
                  <label for="image_two" class="form-control-label"><b>Image Two</b> </label><br/>
                    <img src="{{$product->image_two}}" height="100" width="100" style="border-radius: 5px;" />
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group" style="display: inline-grid;">
                  <label for="image_three" class="form-control-label"><b>Image Three</b> </label><br/>
                    <img src="{{$product->image_three}}" height="100" width="100" style="border-radius: 5px;" />
                </div>
              </div>
            </div><!-- row -->
            <hr />
            <div class="row my-5">
                <div class="col-lg-2">
                    <label class=""><b>Main Slider</b></label><br/>
                        @if($product->main_slider == 1)
                        <span class="badge badge-success">Active</span>
                        @else
                        <span class="badge badge-danger">Inactive</span>
                        @endif
                </div>
                <div class="col-lg-2">
                    <label class=""><b>Hot Deal</b></label><br/>
                    @if($product->hot_deal == 1)
                        <span class="badge badge-success">Active</span>
                        @else
                        <span class="badge badge-danger">Inactive</span>
                        @endif
                </div>
                <div class="col-lg-2">
                    <label class=""><b>Best Rated</b></label><br/>
                        @if($product->best_rated == 1)
                        <span class="badge badge-success">Active</span>
                        @else
                        <span class="badge badge-danger">Inactive</span>
                        @endif
                </div>
                <div class="col-lg-2">
                    <label class=""><b>Mid Slider</b></label><br/>
                        @if($product->mid_slider == 1)
                        <span class="badge badge-success">Active</span>
                        @else
                        <span class="badge badge-danger">Inactive</span>
                        @endif
                </div>
                <div class="col-lg-2">
                    <label class=""><b>Host New</b></label><br/>
                        @if($product->host_new == 1)
                        <span class="badge badge-success">Active</span>
                        @else
                        <span class="badge badge-danger">Inactive</span>
                        @endif
                </div>
                <div class="col-lg-2">
                    <label class=""><b>Trend Product</b></label><br/>
                        @if($product->trend == 1)
                        <span class="badge badge-success">Active</span>
                        @else
                        <span class="badge badge-danger">Inactive</span>
                        @endif
                </div>
            </div>
            <hr/>
            <div class="form-layout-footer">
              <a href="{{ route('backoffice.product.index') }}" class="btn btn-secondary">Back</a>
            </div><!-- form-layout-footer -->
        </div><!-- card -->
      </div><!-- sl-pagebody -->
    </div>

@endsection