<!-- jquery -->
<script src="/assets/js/jquery.min.js"></script>
<!-- popper Min Js -->
<script src="/assets/js/popper.min.js"></script>
<!-- Bootstrap Min Js -->
<script src="/assets/js/bootstrap.min.js"></script>
<!-- Fontawesome-->
<script src="/assets/js/all.min.js"></script>
<!-- metis menu -->
<script src="/assets/plugins/metismenu-3.0.4/assets/js/metismenu.js"></script>
<script src="/assets/plugins/metismenu-3.0.4/assets/js/mm-vertical-hover.js"></script>
<!-- nice scroll bar -->
<script src="/assets/plugins/scrollbar/jquery.nicescroll.min.js"></script>
<script src="/assets/plugins/scrollbar/scroll.active.js"></script>
<!-- counter -->
<script src="/assets/plugins/counter/js/counter.js"></script>
<!-- chart -->
<script src="/assets/plugins/chartjs-bar-chart/Chart.min.js"></script>
<script src="/assets/plugins/chartjs-bar-chart/chart.js"></script>
<!-- pie chart -->
<script src="/assets/plugins/pie_chart/chart.loader.js"></script>
<script src="/assets/plugins/pie_chart/pie.active.js"></script>
<!-- Main js -->
<script src="/assets/js/main.js"></script>
<!-- Notify Alert plugin -->
<script src="/assets/js/sweetalert2.js"></script>
<script src="/assets/js/toastr.min.js"></script>

{!! Toastr::message() !!}
@section('script')

@show