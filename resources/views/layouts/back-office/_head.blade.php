<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
<meta name="author" content="{{ config('app.author') }}">
<meta name="keywords" content="{{ config('app.keywords') }}">
<meta name="description" content="{{ isset($description) ? $description : config('app.description') }}"/>
<meta name="user-name" content="{{Auth::check() ? Auth::user()->first_name : 'name' }}">
<meta property="og:type" name="og:type" content="site"/>
<meta property="og:country" content="{{ config('app.country') }}"/>
<meta property="og:url" name="og:url" content="{{ request()->url() }}"/>
<meta property="og:title" name="og:title" content="{{ isset($title) ? $title : config('app.title') }}">
<meta property="og:description" name="og:description" content="{{ isset($description) ? $description : config('app.description') }}">
<link rel="icon" href="/assets/images/favicon.png" >
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">    
<!--Page title-->
<title>@yield('title')</title>
<!--bootstrap-->
<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
<!--font awesome-->
<link rel="stylesheet" href="/assets/css/all.min.css">
<!-- metis menu -->
<link rel="stylesheet" href="/assets/plugins/metismenu-3.0.4/assets/css/metisMenu.min.css">
<link rel="stylesheet" href="/assets/plugins/metismenu-3.0.4/assets/css/mm-vertical-hover.css">
<!-- chart -->
<!-- <link rel="stylesheet" href="/assets/plugins/chartjs-bar-chart/chart.css"> -->
<!--Custom CSS-->
<link rel="stylesheet" href="/assets/css/style.css">
<!-- Sweet Alert plugin -->
<link rel="stylesheet" href="/assets/css/sweetalert2.css">
<link rel="stylesheet" href="/assets/css/animate.css">
<!-- Notify Alert plugin -->
<link rel="stylesheet" href="/assets/css/toastr.min.css">
<link rel="stylesheet" href="/assets/css/select2.css">

@section('style')
@show