<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
<meta name="author" content="{{ config('app.author') }}">
<meta name="keywords" content="{{ config('app.keywords') }}">
<meta name="description" content="{{ isset($description) ? $description : config('app.description') }}"/>
<meta name="user-name" content="{{Auth::check() ? Auth::user()->first_name : 'name' }}">
<meta property="og:type" name="og:type" content="site"/>
<meta property="og:country" content="{{ config('app.country') }}"/>
<meta property="og:url" name="og:url" content="{{ request()->url() }}"/>
<meta property="og:title" name="og:title" content="{{ isset($title) ? $title : config('app.title') }}">
<meta property="og:description" name="og:description" content="{{ isset($description) ? $description : config('app.description') }}">
<link rel="icon" href="/assets/images/favicon.png">
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">    
<!--Page title-->
<title>@yield('title')</title>
<!-- Multiple Tags Input css -->
<link href="https://cdn.jsdelivr.net/bootstrap.tagsinput/0.8.0/bootstrap-tagsinput.css" rel="stylesheet"/>
<!-- vendor css -->
<link href="/assets/admin/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="/assets/admin/lib/Ionicons/css/ionicons.css" rel="stylesheet">
<link href="/assets/admin/lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
<link href="/assets/admin/lib/rickshaw/rickshaw.min.css" rel="stylesheet">
<!-- Datatables CSS -->
<link href="/assets/admin/lib/highlightjs/github.css" rel="stylesheet">
<link href="/assets/admin/lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="/assets/admin/lib/select2/css/select2.min.css" rel="stylesheet">
<!-- Text Editor css -->
<link href="/assets/admin/lib/summernote/summernote-bs4.css" rel="stylesheet">
<!-- Starlight CSS -->
<link rel="stylesheet" href="/assets/admin/css/starlight.css">
<!-- Sweet Alert plugin -->
<link rel="stylesheet" href="/assets/css/sweetalert2.css">
<link rel="stylesheet" href="/assets/css/animate.css">
<!-- Notify Alert plugin -->
<link rel="stylesheet" href="/assets/css/toastr.min.css">
<link rel="stylesheet" href="/assets/css/select2.css">

@section('style')
@show