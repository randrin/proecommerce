<!-- Multiple Tags Input css -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bootstrap.tagsinput/0.8.0/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript">
      $(document).ready(function(){
     $('select[name="category_id"]').on('change',function(){
          var category_id = $(this).val();
          if (category_id) {
            $.ajax({
              url: "{{ url('/backoffice/get/subcategories/') }}/"+category_id,
              type:"GET",
              dataType:"json",
              success:function(data) { 
              var d =$('select[name="subcategory_id"]').empty();
              $.each(data, function(key, value){
              $('select[name="subcategory_id"]').append('<option value="'+ value.id + '">' + value.name + '</option>');
              });
              },
            });
          }else{
            alert('danger');
          }
        });
      });
 </script>
<!--/ Multiple Tags Input css -->
<script src="/assets/admin/lib/jquery/jquery.js"></script>
<script src="/assets/admin/lib/popper.js/popper.js"></script>
<script src="/assets/admin/lib/bootstrap/bootstrap.js"></script>
<script src="/assets/admin/lib/jquery-ui/jquery-ui.js"></script>
<script src="/assets/admin/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
<script src="/assets/admin/lib/jquery.sparkline.bower/jquery.sparkline.min.js"></script>
<script src="/assets/admin/lib/d3/d3.js"></script>
<script src="/assets/admin/lib/rickshaw/rickshaw.min.js"></script>
<script src="/assets/admin/lib/chart.js/Chart.js"></script>
<script src="/assets/admin/lib/Flot/jquery.flot.js"></script>
<script src="/assets/admin/lib/Flot/jquery.flot.pie.js"></script>
<script src="/assets/admin/lib/Flot/jquery.flot.resize.js"></script>
<script src="/assets/admin/lib/flot-spline/jquery.flot.spline.js"></script>
<!-- Text Editor css -->
<script src="/assets/admin/lib/medium-editor/medium-editor.js"></script>
<script src="/assets/admin/lib/summernote/summernote-bs4.min.js"></script>
<script>
      $(function(){
        'use strict';

        // Inline editor
        var editor = new MediumEditor('.editable');

        // Summernote editor
        $('#summernote').summernote({
          height: 150,
          tooltip: false
        })
      });
    </script>
<script src="/assets/admin/js/starlight.js"></script>
<script src="/assets/admin/js/ResizeSensor.js"></script>
<script src="/assets/admin/js/dashboard.js"></script>
<script src="/assets/admin/lib/highlightjs/highlight.pack.js"></script>
<!-- Datatables js -->
<script src="/assets/admin/lib/datatables/jquery.dataTables.js"></script>
<script src="/assets/admin/lib/datatables-responsive/dataTables.responsive.js"></script>
<script src="/assets/admin/lib/select2/js/select2.min.js"></script>
<!-- Main js -->
<script src="/assets/js/main.js"></script>
<!-- Notify Alert plugin -->
<script src="/assets/js/sweetalert2.js"></script>
<script src="/assets/js/toastr.min.js"></script>
<script type="text/javascript">
      $(function(){
        'use strict';

        $('#datatable1').DataTable({
          responsive: true,
          language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page',
          }
        });
        
        // Select2
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

      });
    </script>
    <script type="text/javascript">
      $(document).on("click", "#delete", function(e) {
        e.preventDefault();
        var link = $(this).attr('href');

        Swal.fire({
          title: 'Are you sure you want to delete?',
          text: "You won't be able to revert this!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
          if (result.value) {
          window.location.href = link
          Swal.fire(
            'Deleted!',
            'Your component has been deleted.',
            'success'
          )
        }
      })
    });  
    </script>
{!! Toastr::message() !!}
@section('script')

@show