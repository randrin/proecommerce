<!-- jquery -->
<script src="/assets/front-office/js/jquery-3.3.1.min.js"></script>
<!-- popper Min Js -->
<script src="/assets/front-office/styles/bootstrap4/popper.js"></script>
<!-- Bootstrap Min Js -->
<script src="/assets/front-office/styles/bootstrap4/bootstrap.min.js"></script>
<script src="/assets/front-office/plugins/greensock/TweenMax.min.js"></script>
<script src="/assets/front-office/plugins/greensock/TimelineMax.min.js"></script>
<script src="/assets/front-office/plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="/assets/front-office/plugins/greensock/animation.gsap.min.js"></script>
<script src="/assets/front-office/plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="/assets/front-office/plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="/assets/front-office/plugins/slick-1.8.0/slick.js"></script>
<script src="/assets/front-office/plugins/easing/easing.js"></script>
<script src="/assets/front-office/js/custom.js"></script>
<!-- Notify Alert plugin -->
<script src="/assets/js/sweetalert2.js"></script>
<script src="/assets/js/toastr.min.js"></script>

{!! Toastr::message() !!}
@section('script')

@show