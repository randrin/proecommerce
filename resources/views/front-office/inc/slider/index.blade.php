<div class="banner_2">
    <div class="banner_2_background" style="background-image:url(/assets/front-office/images/banner_2_background.jpg)"></div>
    <div class="banner_2_container">
        <div class="banner_2_dots"></div>
        <!-- Banner 2 Slider -->

        <div class="owl-carousel owl-theme banner_2_slider">

            @foreach ($slider_products as $slider)
            <!-- Banner 2 Slider Item -->
            <div class="owl-item">
                <div class="banner_2_item">
                    <div class="container fill_height">
                        <div class="row fill_height">
                            <div class="col-lg-4 col-md-6 fill_height">
                                <div class="banner_2_content">
                                    <div class="banner_2_category"><h4>{{$slider->category->name}}</h4></div>
                                    <div class="banner_2_title">{{$slider->subcategory->name}}</div>
                                    <div class="banner_2_text">{{strip_tags($slider->product_details)}}</div><br/>
                                    <div class="deals_item_price ml-auto">${{ $slider->selling_price }}</div>
                                    <div class="rating_r rating_r_4 banner_2_rating"><i></i><i></i><i></i><i></i><i></i></div>
                                    <div class="button banner_2_button"><a href="#">Explore</a></div>
                                </div>
                                
                            </div>
                            <div class="col-lg-8 col-md-6 fill_height">
                                <div class="banner_2_image_container">
                                    <div class="banner_2_image"><img src="{{ $slider->image_one }}" alt="{{ $slider->product_name }}" style="width: 300px; height: 300px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>			
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>