<div class="banner">
		<div class="banner_background" style="background-image:url(/assets/front-office/images/banner_background.jpg)"></div>
		<div class="container fill_height">
			<div class="row fill_height">
				<div class="banner_product_image"><img src="{{ $products->image_one }}" alt="{{ $products->product_name }}" style="top: -30px; position: relative;"></div>
				<div class="col-lg-5 offset-lg-4 fill_height">
					<div class="banner_content">
						<h1 class="banner_text">{{ $products->product_name }}</h1>
						<div class="banner_price">
                            @if($products->discount_price)
                                <span>${{ $products->selling_price }}</span>
                                ${{ $products->discount_price }}
                                @else
                                ${{ $products->selling_price }}
                            @endif
                        </div>
						<div class="banner_product_name">{{ $products->brand_name }}</div>
						<div class="button banner_button"><a href="#">Shop Now</a></div>
					</div>
				</div>
			</div>
		</div>
    </div>
    