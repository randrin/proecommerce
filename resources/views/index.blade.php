@extends('inc.front-office.main')
<?php $item = htmlspecialchars(config('app.name'));?>
@section('title', 'Store | '.$item)

@section('style')
@endsection

@section('init')
    <!-- Site wrapper -->
@endsection

@section('content')

@guest
    @include('front-office.index')
    @else
    @include('home')
@endguest
        
@endsection

@section('script')

@endsection
