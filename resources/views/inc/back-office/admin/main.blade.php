<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
@include('layouts.back-office.admin._head')

    @yield('style')
</head>
<body>
    @guest
    @else 
       @include('back-office.inc.navleft')
        @include('back-office.inc.navtop')
        @include('back-office.inc.navright')
    @endguest
    
@section('content')

@show

@include('layouts.back-office.admin._script')

@yield('script')
</body>
</html>
