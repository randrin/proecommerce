<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
@include('layouts.back-office._head')

    @yield('style')
</head>
<body id="page-top">
    <!-- preloader -->
        <div class="preloader">
            <img src="/assets/images/preloader.gif" alt="">
        </div>


@section('content')


@show

@include('layouts.back-office._script')

@yield('script')
</body>
</html>
