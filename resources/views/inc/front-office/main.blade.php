<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
@include('layouts.front-office._head')

    @yield('style')
</head>
<body oncontextmenu="return false">

@section('content')

@show

@include('layouts.front-office._script')

@yield('script')
</body>
</html>
