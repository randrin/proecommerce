<?php

namespace App\Services;

use App\Http\Requests\CategoryRequest;
use App\Model\Category;

class CategoriesService {

    public static function setValuesCategories(CategoryRequest $request, Category $category)
    {
        $category->name = $request->name;
        $category->icon = $request->icon;
        return $category;
    }

    public static function setStatusCategory(String $action, Category $category)
    {
        if($action === 'activate') {
            $category->status = '1';
        } else {
            $category->status = '0';
        }
        return $category;
    }
}
