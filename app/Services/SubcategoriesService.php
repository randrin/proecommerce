<?php

namespace App\Services;

use App\Http\Requests\SubcategoryRequest;
use App\Model\Subcategory;

class subcategoriesService {

    public static function setValuesSubcategories(SubcategoryRequest $request, Subcategory $subcategory)
    {
        $subcategory->name = $request->name;
        $subcategory->category_id = $request->category_id;
        return $subcategory;
    }

    public static function setStatusSubcategory(String $action, Subcategory $subcategory)
    {
        if ($action === 'activate') {
            $subcategory->status = '1';
        } else {
            $subcategory->status = '0';
        }
        return $subcategory;
    }
}