<?php

namespace App\Services;

use App\Http\Requests\CouponRequest;
use App\Model\Coupon;

class CouponsService
{

    public static function setValuesCoupons(CouponRequest $request, Coupon $coupon)
    {
        $coupon->name = $request->name;
        $coupon->discount = $request->discount;
        return $coupon;
    }

    public static function setStatusCoupon(String $action, Coupon $coupon)
    {
        if ($action === 'activate') {
            $coupon->status = '1';
        } else {
            $coupon->status = '0';
        }
        return $coupon;
    }
}
