<?php

namespace App\Services;

use App\Http\Requests\BrandRequest;
use App\Model\Brand;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

class BrandsService {
    public function setValuesBrands(String $action, BrandRequest $request, Brand $brand)
    {
        $brand->name = $request->name;
        if($action === 'update') {
            $oldLogo = $brand->logo;
            if ($request->logo != $oldLogo) {
                $this->saveLogoBrand($request, $brand);

                // Ici je suprimme le logo existant
                File::delete(public_path($oldLogo));
            }
        } else {
            if ($request->logo) {
                $this->saveLogoBrand($request, $brand);
            }
        }
        return $brand;
    }

    public function saveLogoBrand(BrandRequest $request, Brand $brand) {
        $namefile = sha1(date('YmdHis') . str_random(30));

        //$name = $namefile . '.' . explode('/', explode(':', substr($request->logo, 0, strpos($request->logo, ';')))[1])[1];

        $name = $namefile . '.' . strtolower($request->file('logo')->getClientOriginalExtension());

        $dir = 'assets/img/brand/';
        if (!file_exists($dir)) {
            mkdir($dir, 0775, true);
        }
        $destinationPath = public_path("assets/img/brand/{$name}");
        Image::make($request->logo)->fit(400, 400)->save($destinationPath);

        //Save Image to database
        $myfilename = "/assets/img/brand/{$name}";
        $brand->logo = $myfilename;
    }

    public static function setStatusBrand(String $action, Brand $brand)
    {
        if ($action === 'activate') {
            $brand->status = '1';
        } else {
            $brand->status = '0';
        }
        return $brand;
    }

    public static function deleteBrand(Brand $brand)
    {
        $oldFilename = $brand->logo;
        File::delete(public_path($oldFilename));
        return $brand;
    }
}