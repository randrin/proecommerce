<?php

namespace App\Services;

use App\Http\Requests\ProductRequest;
use App\Model\Product;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

class ProductsService {

    public static function setValuesProducts(Request $request, Product $product, String $action)
    {
        $product->product_name = $request->product_name;
        $product->product_code = $request->product_code;
        $product->product_quantity = $request->product_quantity;
        $product->category_id = $request->category_id;
        $product->subcategory_id = $request->subcategory_id;
        $product->brand_id = $request->brand_id;
        $product->product_size = $request->product_size;
        $product->product_color = $request->product_color;
        $product->product_details = $request->product_details;
        $product->selling_price = $request->selling_price;
        $product->discount_price = $request->discount_price;
        $product->video_link = $request->video_link;
        $product->main_slider = $request->main_slider;
        $product->hot_deal = $request->hot_deal;
        $product->best_rated = $request->best_rated;
        $product->mid_slider = $request->mid_slider;
        $product->host_new = $request->host_new;
        $product->trend = $request->trend;
        if($action == 'store') {
            $product->image_one = $request->file('image_one');
            $product->image_two = $request->file('image_two');
            $product->image_three = $request->file('image_three');
        }
        return $product;
    }

    public function saveProductImages(Product $product) {
        if ($product->image_one && $product->image_two && $product->image_three) {
            $product->image_one = $this->retrieveImageProduct($product->image_one);
            $product->image_two = $this->retrieveImageProduct($product->image_two);
            $product->image_three = $this->retrieveImageProduct($product->image_three);
            return $product;
        }
        return $product;
    }

    public static function retrieveImageProduct($productImage) {
        $namefile = sha1(date('YmdHis') . str_random(30));

        //$name = $namefile . '.' . explode('/', explode(':', substr($request->logo, 0, strpos($request->logo, ';')))[1])[1];

        $name = $namefile . '.' . strtolower($productImage->getClientOriginalExtension());

        $dir = 'assets/img/products/';
        if (!file_exists($dir)) {
            mkdir($dir, 0775, true);
        }
        $destinationPath = public_path("assets/img/products/{$name}");
        Image::make($productImage)->fit(400, 400)->save($destinationPath);

        //Save Image to database
        $myfilename = "/assets/img/products/{$name}";
        $productImage = $myfilename;
        return $productImage;
    }

    public static function getPercentageDiscountProduct(Product $product) {
        if($product->discount_price) {
            $discount = ceil((($product->selling_price - $product->discount_price)/$product->selling_price)*100);
            $product->discount_percentage = $discount;
            return $product;
        }
        return $product;
    }

    public static function generateProductCode() {
        return 'PROD-' .mt_rand(1000000, 9999999). '-' .mt_rand(1000000, 9999999);
    }

    public static function setStatusProduct(String $action, Product $product)
    {
        if ($action === 'activate') {
            $product->status = '1';
        } else {
            $product->status = '0';
        }
        return $product;
    }

    public static function deleteProduct(Product $product)
    {
        $oldFilename1 = $product->image_one;
        File::delete(public_path($oldFilename1));
        $oldFilename2 = $product->image_two;
        File::delete(public_path($oldFilename2));
        $oldFilename3 = $product->image_three;
        File::delete(public_path($oldFilename3));
        return $product;
    }

}