<?php

namespace App\Http\Controllers;

use App\Model\Category;
use App\Model\Product;
use App\Model\Subcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['welcome']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function welcome() {
        $categories = Category::where('status', 1)->orderBy('created_at', 'desc')->get();
        $subcategories = Subcategory::where('status', 1)->orderBy('created_at', 'desc')->get();
        $products = Product::orderBy('created_at', 'desc')
            ->join('brands', 'products.brand_id', 'brands.id')
            ->select('products.*', 'brands.name as brand_name')
            ->where('main_slider', 1)
            ->first();
        $featured_products = Product::orderBy('created_at', 'desc')->where('status', 1)->limit(8)->get();
        $trend_products = Product::orderBy('created_at', 'desc')->where(['status' => 1, 'trend' => 1])->limit(8)->get();
        $best_products = Product::orderBy('created_at', 'desc')->where(['status' => 1, 'best_rated' => 1])->limit(8)->get();
        $hot_products = Product::orderBy('created_at', 'desc')->where(['status' => 1, 'hot_deal' => 1])->limit(3)->get();
        $slider_products = Product::orderBy('created_at', 'desc')->where(['status' => 1, 'main_slider' => 1])->limit(3)->get();
        //return response()->json($hot_products);
        return view('index', compact('categories', 'subcategories', 'products', 'featured_products', 'trend_products', 'best_products', 'hot_products', 'slider_products'));
    }

    public function logout() {
        Auth::logout();
        toastr()->success('<b>Logout Successfully.</b>', '<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>');
        return redirect()->route('login');
    }
}
