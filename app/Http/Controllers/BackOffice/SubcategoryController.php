<?php

namespace App\Http\Controllers\BackOffice;

use App\Model\Subcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SubcategoryRequest;
use App\Model\Category;
use App\Services\SubcategoriesService;

class SubcategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::where('status', 1)->orderBy('created_at', 'desc')->get();
        $subcategories = Subcategory::orderBy('created_at', 'desc')
                                ->join('categories', 'subcategories.category_id', 'categories.id')
                                ->select('subcategories.*', 'categories.name as category_name')
                                ->get();
        return view('back-office.subcategory.index', compact('categories', 'subcategories'));
    }

    public function getSubcategoriesByCategory($category_id) {
        $subcategories = Subcategory::where(['category_id' => $category_id, 'status' => 1])->get();
        return json_encode($subcategories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubcategoryRequest $request)
    {
        $subcategory = SubcategoriesService::setValuesSubcategories($request, new Subcategory());
        $subcategory->save();
        toastr()->success('<b>Sub Category created Successfully.</b>', '<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>');
        return redirect()->back();
    }

    public function activate($id)
    {
        $subcategory = Subcategory::where('id', $id)->first();
        $subcategory = SubcategoriesService::setStatusSubcategory('activate', $subcategory);
        $subcategory->update();
        toastr()->success('<b>Sub Category Activated Successfully.</b>', '<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>');
        return redirect()->back();
    }

    public function desactivate($id)
    {
        $subcategory = Subcategory::where('id', $id)->first();
        $subcategory = SubcategoriesService::setStatusSubcategory('desactivate', $subcategory);
        $subcategory->update();
        toastr()->success('<b>Sub Category Desactivated Successfully.</b>', '<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Subcategory  $subcategory
     * @return \Illuminate\Http\Response
     */
    public function show(Subcategory $subcategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Subcategory  $subcategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::where('status', 1)->orderBy('created_at', 'desc')->get();
        $subcategory = Subcategory::where('id', $id)->first();
        return view('back-office.subcategory.edit', compact('subcategory', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Subcategory  $subcategory
     * @return \Illuminate\Http\Response
     */
    public function update(SubcategoryRequest $request, $id)
    {
        $subcategory = Subcategory::where('id', $id)->first();
        $subcategory = SubcategoriesService::setValuesSubcategories($request, $subcategory);
        $subcategory->update();
        toastr()->success('<b>Sub Category updated Successfully.</b>', '<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>');
        return redirect()->route('backoffice.subcategory.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Subcategory  $subcategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subcategory = Subcategory::findOrFail($id);
        $subcategory->delete();
        return redirect()->back();
    }
}
