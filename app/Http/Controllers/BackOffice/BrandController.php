<?php

namespace App\Http\Controllers\BackOffice;

use App\Model\Brand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\BrandRequest;
use App\Services\BrandsService;

class BrandController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brand::orderBy('created_at','desc')->get();
        return view('back-office.brand.index', compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BrandRequest $request)
    {
        $brandsService = new BrandsService();
        $brand = $brandsService->setValuesBrands('store', $request, new Brand());
        $brand->save();
        toastr()->success('<b>Brand created Successfully.</b>', '<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>');
        return redirect()->back();
    }

    public function activate($id)
    {
        $brand = Brand::where('id', $id)->first();
        $brand = BrandsService::setStatusBrand('activate', $brand);
        $brand->update();
        toastr()->success('<b>Brand Activated Successfully.</b>', '<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>');
        return redirect()->back();
    }

    public function desactivate($id)
    {
        $brand = Brand::where('id', $id)->first();
        $brand = BrandsService::setStatusBrand('desactivate', $brand);
        $brand->update();
        toastr()->success('<b>Brand Desactivated Successfully.</b>', '<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand = Brand::where('id', $id)->first();
        return view('back-office.brand.edit', compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(BrandRequest $request, $id)
    {
        $brandsService = new BrandsService();
        $brand = Brand::where('id', $id)->first();
        $brand = $brandsService->setValuesBrands('update', $request, $brand);
        $brand->update();
        toastr()->success('<b>Brand Updated Successfully.</b>', '<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>');
        return redirect()->route('backoffice.brand.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand = Brand::findOrFail($id);
        $brand = BrandsService::deleteBrand($brand);
        $brand->delete();
        return redirect()->back();
    }
}
