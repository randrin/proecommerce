<?php

namespace App\Http\Controllers\BackOffice;

use App\Model\Product;
use App\Model\Brand;
use App\Model\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Model\Subcategory;
use App\Services\ProductsService;

class ProductController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('created_at', 'desc')
        ->join('categories', 'products.category_id', 'categories.id')
        ->join('brands', 'products.brand_id', 'brands.id')
        ->select('products.*', 'categories.name as category_name', 'brands.name as brand_name')
        ->get();
        //return response()->json($products);
        return view('back-office.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('status', 1)->orderBy('created_at', 'desc')->get();
        $brands = Brand::where('status', 1)->orderBy('created_at', 'desc')->get();
        $product_code =  ProductsService::generateProductCode();
        return view('back-office.product.create', compact('categories', 'brands', 'product_code'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $productsService = new ProductsService(); 
        $request->product_code = ProductsService::generateProductCode();
        $product = $productsService::setValuesProducts($request, new Product(), 'store');
        $product = $productsService->saveProductImages($product);
        $product = $productsService->getPercentageDiscountProduct($product);
        $product->save();
        toastr()->success('<b>Product created Successfully.</b>', '<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>');
        return redirect()->route('backoffice.product.index');
    }

    public function activate($id)
    {
        $product = Product::where('id', $id)->first();
        $product = ProductsService::setStatusProduct('activate', $product);
        $product->update();
        toastr()->success('<b>Product Activated Successfully.</b>', '<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>');
        return redirect()->back();
    }

    public function desactivate($id)
    {
        $product = Product::where('id', $id)->first();
        $product = ProductsService::setStatusProduct('desactivate', $product);
        $product->update();
        toastr()->success('<b>Product Desactivated Successfully.</b>', '<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::orderBy('created_at', 'desc')
        ->join('categories', 'products.category_id', 'categories.id')
        ->join('subcategories', 'products.subcategory_id', 'subcategories.id')
        ->join('brands', 'products.brand_id', 'brands.id')
        ->select('products.*', 'categories.name as category_name', 'subcategories.name as subcategory_name', 'brands.name as brand_name')
        ->where('products.id', $id)
        ->first();
        //return response()->json($product);
        return view('back-office.product.view', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::where('status', 1)->orderBy('created_at', 'desc')->get();
        $subcategories = Subcategory::where('status', 1)->orderBy('created_at', 'desc')->get();
        $brands = Brand::where('status', 1)->orderBy('created_at', 'desc')->get();
        $product = Product::orderBy('created_at', 'desc')
        ->join('categories', 'products.category_id', 'categories.id')
        ->join('brands', 'products.brand_id', 'brands.id')
        ->select('products.*', 'categories.name as category_name', 'brands.name as brand_name')
        ->where('products.id', $id)
        ->first();
        //return response()->json($products);
        return view('back-office.product.edit', compact('product', 'categories', 'brands', 'subcategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request);
        $productsService = new ProductsService();
        $product = Product::where('id', $id)->first();
        $product = $productsService::setValuesProducts($request, $product, 'update');
        dd($product);
        $product = $productsService->saveProductImages($product);
        $product->update();
        toastr()->success('<b>Product updated Successfully.</b>', '<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>');
        return redirect()->route('backoffice.product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product = ProductsService::deleteProduct($product);
        $product->delete();
        return redirect()->back();
    }
}
