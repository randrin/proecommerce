<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function changePassword() {
        return view('admin.auth.passwords.change');
    }

    public function updatePassword(Request $request) {
        $password = Auth::user()->password;
        $oldpass = $request->oldpass;
        $newpass = $request->password;
        $confirm = $request->password_confirmation;
        if (Hash::check($oldpass, $password)) {
            if ($newpass === $confirm) {
                $user = Admin::find(Auth::id());
                $user->password = Hash::make($request->password);
                $user->save();
                Auth::logout();
                toastr()->success('<b>Password Changed Successfully ! Now Login with Your New Password.</b>', '<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>');
                return redirect()->route('admin.login');
            } else {
                toastr()->error('<b>New password and Confirm Password not matched !.</b>', '<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>');
                return redirect();
            }
        } else {
            toastr()->error('<b>Old Password not matched !.</b>', '<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>');
            return redirect()->back();
        }
    }
}
