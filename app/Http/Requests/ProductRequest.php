<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_name' => 'required|string|unique:products|min:3|max:50'.$this->id,
            'product_code' => 'required|unique:products',
            'product_quantity' => 'required|numeric',
            'category_id' => 'required|numeric',
            'subcategory_id' => 'required|numeric',
            'brand_id' => 'required|numeric',
            'product_size' => 'required',
            'product_color' => 'required',
            'product_details' => 'required|string|min:5|max:10000',
            'selling_price' => 'required|numeric',
            'image_one' => 'required|file',
            'image_two' => 'required|file',
            'image_three' => 'required|file',
        ];
    }
}
